import QtQuick 2.6
import test 1.0

Rectangle {
    id: root
    width: 1200
    height: 1100

    property color col: "black"

    // 0 for QtQuick, 1 for paintedItem, 2 for canvas
    property int solution: 2
    property int rectangleSize: 4
    property int numberOfRectangles: 10000

    ColorAnimation on col {
        from: "red"
        to: "green"
        duration: 8000
        loops: 100
    }

    Repeater {
        model: root.numberOfRectangles

        Loader {
            width: root.rectangleSize
            height:root.rectangleSize
            opacity: 0.9 // even if overlap, drawing is needed
            sourceComponent: root.solution == 0 ? qtquickComponent :
                                  root.solution == 1 ? paintedComponent : canvasComponent
        }
    }

    // QtQuick OpenGL based solution
    Component {
        id: qtquickComponent
        Rectangle {
            color: root.col
        }
    }

    // QQuickPaintedItem based solution
    Component {
        id: paintedComponent
        PainterRectItem {
            color: root.col
        }
    }

    // Canvas based solution
    Component {
        id: canvasComponent
        Canvas {
            id: canvasRect
            Connections {
                target: root
                onColChanged: canvasRect.requestPaint();
            }
            onPaint: {
                var ctx = getContext("2d");
                ctx.fillStyle = root.col;
                ctx.fillRect(0, 0, width, height);
            }
        }
    }
}
