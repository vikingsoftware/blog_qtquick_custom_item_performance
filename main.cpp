#include "painterrectitem.h"

#include <QDebug>
#include <QElapsedTimer>
#include <QGuiApplication>
#include <QQmlEngine>
#include <QQuickView>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<PainterRectItem>("test", 1, 0, "PainterRectItem");

    QQuickView view;
    view.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
    view.show();

    int totalDuration = 0;
    int loops = 0;
    QElapsedTimer timer;
    timer.start();
    QObject::connect(&view, &QQuickView::beforeRendering, [&](){
        totalDuration += timer.elapsed();
        ++loops;
        if (totalDuration > 10*1000) {
            qDebug() << (1000.0 * loops) / totalDuration << "fps (average of about last 10 second)";
            totalDuration = 0;
            loops = 0;
        }
        timer.restart();
    });

    return app.exec();
}
